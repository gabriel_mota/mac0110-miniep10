function max_sum_submatrix(matrix)
	tamanho = size(matrix)[1]
	maior = 0
	resp = []
	for i in 1:tamanho
		for j in 1:tamanho
			atual = submatrix(matrix, li, lf, ci, cf)
			valorAtual = sum_matrix(atual)
			if valorAtual == maior
				push!(resp, matrix)
			elseif valorAtual > maior
				maior = valorAtual
				resp = []
				push!(resp, atual)
			end
		end
	end
	return resp
end

function submatrix(matrix, li, lf, ci, cf)
	v = zeros(lf-li, cf-ci)
	contI = 1
	for i in li:lf
		contJ = 1
		for j in ci:cf
			v[contI, contJ] = matrix[ci, cf]
			contJ += 1
		end
		contI += 1
	end
	return v
end

function sum_matrix(matrix, li, lf, ci, cf)
	tamanho = size(matrix)[1]
	sum = 0
	for i in 1:tamanho
		for j in 1:tamanho
			sum += matrix[i][j]
		end
	end
	return sum
end
